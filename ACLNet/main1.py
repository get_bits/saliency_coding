from __future__ import division
from keras.layers import Input
from keras.models import Model
import os
import numpy as np
from config import *
from utilities import preprocess_images, postprocess_predictions
from models import acl_vgg
from scipy.misc import imread, imsave
from math import ceil
import cv2


def readyuv(filename, width, height, framenum,videos_test_path):

    with open(filename, 'rb') as fp:
        for i in range(1,framenum+1):
            print("current frame: %d" % i)
            oneframe_I420 = np.zeros((height * 3 // 2, width), np.uint8)
            for j in range(height * 3 // 2):
                for k in range(width):
                    oneframe_I420[j, k] = int.from_bytes(fp.read(1), byteorder='little', signed=False)
            oneframe_RGBA = cv2.cvtColor(oneframe_I420, cv2.COLOR_YUV2RGB_I420)

            imsave(videos_test_path+str(i)+'.png', oneframe_RGBA)


def get_test(video_test_path):
    images = [video_test_path + f for f in os.listdir(video_test_path) if
              f.endswith(('.jpg', '.jpeg', '.png'))]
    images.sort()
    start = 0
    while True:
        Xims = np.zeros((1, num_frames, shape_r, shape_c, 3))
        X = preprocess_images(images[start:min(start + num_frames, len(images))], shape_r, shape_c)
        Xims[0, 0:min(len(images)-start, num_frames), :] = np.copy(X)
        yield Xims  #
        start = min(start + num_frames, len(images))


#if __name__ == '__main__':
def runn():
    phase = 'test'
    if phase == 'train':
        x = Input(batch_shape=(None, None, shape_r, shape_c, 3))
        stateful = False
    else:
        x = Input(batch_shape=(1, None, shape_r, shape_c, 3))
        stateful = True

    #注意使用绝对路径
    videos_test_path = 'E:/saliency_coding/data/BasketballPass_416x240_50/'
    output_folder = videos_test_path+'saliency/'
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    if not os.path.exists(videos_test_path):
        os.makedirs(videos_test_path)

    print("convert yuv into png...")

    readyuv('E:/saliency_coding/data/BasketballPass_416x240_50.yuv', 416, 240, 100, videos_test_path)

    if phase == "test":

        #videos_test_path = 'F:/DHF1K/annotation/testing/'

        # videos = [videos_test_path + f for f in os.listdir(videos_test_path) if os.path.isdir(videos_test_path + f)]
        # videos.sort()
        # nb_videos_test = len(videos)

        m = Model(inputs=x, outputs=acl_vgg(x, stateful))
        print("Loading ACL weights")

        m.load_weights('E:/saliency_coding/ACLNet/ACL.h5')


        images_names = [i for i in os.listdir(videos_test_path) if
                        i.endswith(('.jpg', '.jpeg', '.png'))]
        images_names.sort()


        print("Predicting saliency maps for " + videos_test_path)
        prediction = m.predict_generator(get_test(video_test_path=videos_test_path),
                                         max(ceil(len(images_names) / num_frames), 2))
        predictions = prediction[0]

        for j in range(len(images_names)):
            original_image = imread(videos_test_path + images_names[j])
            x, y = divmod(j, num_frames)
            res = postprocess_predictions(predictions[x, y, :, :, 0], original_image.shape[0],
                                          original_image.shape[1])

            imsave(output_folder + '%s' % images_names[j], res.astype(int))


        # original_image = imread(videos_test_path + images_names[0])
        # x, y = divmod(0, num_frames)
        # res = postprocess_predictions(predictions[x, y, :, :, 0], original_image.shape[0],
        #                               original_image.shape[1])
        #
        # #print(type(res))
        # m.reset_states()
        #
        # return res.tolist()
