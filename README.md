# saliency_coding

#### 介绍
基于显著图的QP工具

#### 软件架构
本项目采用《Revisiting Video Saliency: A Large-scale Benchmark and a New Model》中的ACLNet对视频进行显著性检测
再将显著图馈送到VVC参考软件VTM中来指导QP估计

#### 使用说明

1. 删除./VTM-4.0./build中的文件及文件夹
2. 采用以下命令创建VS工程
    > cd build
    > cmake .. -G "Visual Studio 14 2015 Win64"
3. 在build文件夹中建立workspace文件夹并放入配置文件

4. 下载ACLNet.h5，链接: https://pan.baidu.com/s/1XRzaxwy6dl_bOuZ33bUdCQ 提取码: gbiw，将其放到ACLNet文件夹中
5. 将EncoderApp设为启动项目，配置项目的VC++目录
    > 包含目录：装有tensorflow的python下的include文件夹、opencv的include文件夹
    > 库目录：装有tensorflow的python下的libs文件夹、opencv的lib文件夹
6. 配置项目的链接器输入
    > 附加依赖项：装有tensorflow的python下libs文件夹中python36.lib、opencv_world340d
7. 配置项目的调试属性
    > 命令参数：-c encoder_randomaccess_vtm.cfg
    > 工作目录：当前目录下VTM-4.0/build/workspace
8. 将项目启动项设为“x64”、“Release”
9. 修改encoder_randomaccess_vtm.cfg的配置信息、EncApp.cpp中cv::imread()、computeSaliency()中的路径、main1.py中的路径
10. 运行代码


#### 参与贡献

将全景视频编码结合显著性检测，在保证主观质量相似的情况下大大减小码率

